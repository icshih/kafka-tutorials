package ics.dev.kafka.twitter;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringSerializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TwitterStreams {

    private final Logger logger = LoggerFactory.getLogger(TwitterStreams.class);

    private TwitterStreams() {}

    public KafkaStreams createKafkaStreams() {
        String bootstrapServers = "127.0.0.1:9092";
        // Create Properties
        Properties props = new Properties();
        props.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "demo-kafka-streams");
        props.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
        props.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());

        // Create a topology
        StreamsBuilder builder = new StreamsBuilder();

        // Input topic
        KStream<String, String> inputTopic = builder.stream("twitter-topic");
        // Process the input topic
        KStream<String, String> filteredStream = inputTopic.filter((k, v) -> v.contains("u"));
        // Route the processed data to another topic
        filteredStream.to("twitter-filtered-topic");

        // Build the topology
        return new KafkaStreams(builder.build(), props);
    }

    private void run() {
        // create a kafka streams
        logger.info("Create a Kafka streams");
        KafkaStreams stream = createKafkaStreams();
        stream.start();

        // add a shut down hook
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Shutting down the streams application...");
            stream.close();
            logger.info("End of Application.");
        }));
    }

    public static void main(String[] args) {
        new TwitterStreams().run();
    }



}
