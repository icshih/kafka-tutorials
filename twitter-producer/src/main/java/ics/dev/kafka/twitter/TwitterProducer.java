package ics.dev.kafka.twitter;

import com.google.common.collect.Lists;
import com.twitter.hbc.ClientBuilder;
import com.twitter.hbc.core.Client;
import com.twitter.hbc.core.Constants;
import com.twitter.hbc.core.Hosts;
import com.twitter.hbc.core.HttpHosts;
import com.twitter.hbc.core.endpoint.StatusesFilterEndpoint;
import com.twitter.hbc.core.processor.StringDelimitedProcessor;
import com.twitter.hbc.httpclient.auth.Authentication;
import com.twitter.hbc.httpclient.auth.OAuth1;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class TwitterProducer {

    private final Logger logger = LoggerFactory.getLogger(TwitterProducer.class);

    private TwitterProducer() {}

    public Client createTwitterClient(BlockingQueue<String> msgQueue) {
        String consumerKey = "JbApll5Afbx7WfkoOoufBO0Yh";
        String consumerSecret = "q2OnVRo6Ms1LDqvDhiEm9nHFxcwnwcuHlkJlkKy4NbwICVU0jq";
        String token = "962973983787413504-gCslGGHYLHFaSlHI6yfOHsLe7G9RZI0";
        String secret="SyqcWTra8E902XSEtX18iMa4xjoFa4UYMaObLzfXPgFSJ";
        /* Declare the host you want to connect to, the endpoint, and authentication (basic auth or oauth) */
        Hosts hosebirdHosts = new HttpHosts(Constants.STREAM_HOST);
        StatusesFilterEndpoint hosebirdEndpoint = new StatusesFilterEndpoint();
        List<String> terms = Lists.newArrayList("coronavirus");
        hosebirdEndpoint.trackTerms(terms);

        // These secrets should be read from a config file
        Authentication hosebirdAuth = new OAuth1(consumerKey, consumerSecret, token, secret);

        ClientBuilder builder = new ClientBuilder()
                .name("Hosebird-Client-01")                              // optional: mainly for the logs
                .hosts(hosebirdHosts)
                .authentication(hosebirdAuth)
                .endpoint(hosebirdEndpoint)
                .processor(new StringDelimitedProcessor(msgQueue));

        return builder.build();
    }

    public KafkaProducer<String, String> createKafkaProducer() {
        String bootstrapServers = "127.0.0.1:9092";
        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // optimise data safety
        props.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        // use compression as often as possible
        props.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
        props.setProperty(ProducerConfig.LINGER_MS_CONFIG, "20");
        props.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, Integer.toString(32*1024));
        return new KafkaProducer<>(props);
    }

    private void run() {
        /* Set up your blocking queues: Be sure to size these properly based on expected TPS of your stream */
        BlockingQueue<String> msgQueue = new LinkedBlockingQueue<>(100000);
        logger.info("Hello Twitter!");
        // create a twitter client
        logger.info("Create a twitter client");
        Client client = createTwitterClient(msgQueue);
        client.connect();

        // create a kafka producer
        logger.info("Create a Kafka producer");
        KafkaProducer<String, String> producer = createKafkaProducer();

        // add a shut down hook
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Shutting down the application...");
            client.stop();
            logger.info("Twitter client is stopped.");
            producer.close();
            logger.info("Producer is closed.");
            logger.info("End of Application.");
        }));

        // loop twitter to kafka producer
        logger.info("Loop twitter to Kafka producer");
        // on a different thread, or multiple different threads....
        while (!client.isDone()) {
            String msg = null;
            try {
                msg = msgQueue.poll(5000, TimeUnit.MILLISECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
                client.stop();
            }
            if (msg != null) {
                logger.info(msg);
                producer.send(new ProducerRecord<>("twitter-topic", null, msg), (recordMetadata, e) -> {
                    if (e != null) {
                        logger.error("Something goes wrong, ", e);
                    }
                });
            }
        }
    }

    public static void main(String[] args) {
        new TwitterProducer().run();
    }



}
