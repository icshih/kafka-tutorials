package ics.dev.kafka.elastic;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.List;
import java.util.Properties;

public class ElasticSearchConsumerBasic {

    final Logger logger = LoggerFactory.getLogger(ElasticSearchConsumerBasic.class);

    public ElasticSearchConsumerBasic() {}

    public void run() throws IOException {
        logger.info("Create an ElasticSearch client");
        RestHighLevelClient client = createElasticSearchClient();

        String jsonString = "{\"name\": \"I-Chun\"}";
        IndexRequest request = new IndexRequest("twitter")
                .id("1")
                .source(jsonString, XContentType.JSON);
        IndexResponse response = client.index(request, RequestOptions.DEFAULT);
        String id = response.getId();
        logger.info(id);

        client.close();
    }

    public RestHighLevelClient createElasticSearchClient() {
        String hostname = "kafka-tutorials-774562028.eu-central-1.bonsaisearch.net";
        String username = "zf86kxbpw6";
        String password = "60wkmrnj9g";

        // For Bonsai hosted ElasticSearch only
        CredentialsProvider cp = new BasicCredentialsProvider();
        cp.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials(username, password));

        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(hostname, 443, "https"))
                        .setHttpClientConfigCallback(httpAsyncClientBuilder ->
                                httpAsyncClientBuilder.setDefaultCredentialsProvider(cp)));
    }

    public static void main(String[] args) throws IOException {
        new ElasticSearchConsumerBasic().run();
    }

}
