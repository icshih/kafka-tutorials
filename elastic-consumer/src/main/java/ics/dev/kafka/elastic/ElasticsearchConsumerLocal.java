package ics.dev.kafka.elastic;

import com.google.gson.Gson;
import ics.dev.dm.TweetId;
import org.apache.http.HttpHost;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

public class ElasticsearchConsumerLocal {

    public static void main(String[] args) {
        new ElasticsearchConsumerLocal().run();
    }

    private ElasticsearchConsumerLocal() {
    }

    private void run() {
        final Logger logger = LoggerFactory.getLogger(ElasticsearchConsumerLocal.class);

        String bootstrapServers = "127.0.0.1:9092";
        String groupId = "elastic-consumer-local";
        String topic = "twitter-topic";

        Properties props = new Properties();
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        // Control number of record polled and manually commit offsets
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        props.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "20");


        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        RestHighLevelClient client = createElasticSearchClient();

        logger.info("Creating consumer thread");
        ConsumerRunner runner = new ConsumerRunner(consumer, topic, client);

        logger.info("Starting the consumer thread");
        Thread consumerThread = new Thread(runner);
        consumerThread.start();

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Receive shutdown hook");
            runner.shutdown();
        }));

        try {
            consumerThread.wait();
        } catch (InterruptedException e) {
            logger.warn("Application is interrupted", e);
        } finally {
            logger.info("Application is closing");
        }
    }

    public RestHighLevelClient createElasticSearchClient() {
        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
    }


    public static class ConsumerRunner implements Runnable {

        private final AtomicBoolean closed = new AtomicBoolean(false);
        private final KafkaConsumer<String, String> consumer;
        private final String topic;
        private final RestHighLevelClient client;
        private final Gson gson = new Gson();
        private final Logger logger = LoggerFactory.getLogger(this.getClass());

        public ConsumerRunner(KafkaConsumer<String, String> consumer,
                              String topic,
                              RestHighLevelClient client) {
            this.consumer = consumer;
            this.topic = topic;
            this.client = client;
        }

        @Override
        public void run() {
            try {
                consumer.subscribe(Arrays.asList(topic));

                while(!closed.get()) {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, String> record : records) {
                        String tweetId = extractTweetId(record);
                        logger.info("Insert value to Elasticsearch");
                        IndexRequest request = new IndexRequest("twitter")
                                .id(tweetId)    // Idempotent ingestion
                                .source(record.value(), XContentType.JSON);
                        IndexResponse response = client.index(request, RequestOptions.DEFAULT);
                        logger.info(response.getId());
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    logger.info("Committing offsets...");
                    consumer.commitSync();
                    logger.info("Offsets have been committed");
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                client.close();
            } catch (WakeupException e) {
                logger.warn("Receive shutdown signal.");
                if (!closed.get()) throw e;
            } catch (IOException e) {
                logger.error("Errors in Elasticsearch, ", e);
            } finally {
                consumer.close();

            }
        }

        public String extractTweetId(ConsumerRecord<String, String> record) {
            TweetId tweetId = gson.fromJson(record.value(), TweetId.class);
            return tweetId.getId_str();

        }

        // Shutdown hook which can be called from a separate thread
        public void shutdown() {
            closed.set(true);
            // The wakeup() method is a special method to interrupt consumer.poll()
            // It throws WakeupException
            consumer.wakeup();
        }
    }

}
