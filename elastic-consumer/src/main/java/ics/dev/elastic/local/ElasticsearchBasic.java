package ics.dev.elastic.local;

import org.apache.http.HttpHost;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.rest.RestStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ElasticsearchBasic {

    final Logger logger = LoggerFactory.getLogger(ElasticsearchBasic.class);

    public ElasticsearchBasic() {}

    public void run() throws IOException {
        logger.info("Create an ElasticSearch client");
        final String index = "twitter";
        RestHighLevelClient client = createElasticSearchClient();

        String id = insert(client, index, "{\"name\": \"Begona\", \"city\": \"Paris\"}");
        get(client, index, id);

        client.close();
    }

    public String insert(RestHighLevelClient client, String index, String jsonString) throws IOException {
        IndexRequest request = new IndexRequest(index)
                .source(jsonString, XContentType.JSON);
        IndexResponse response = client.index(request, RequestOptions.DEFAULT);
        String id = response.getId();
        logger.info(id);
        return id;
    }

    public void get(RestHighLevelClient client, String index, String id) throws IOException {
        GetRequest get = new GetRequest(index)
                .id(id);
        try {
            GetResponse response = client.get(get, RequestOptions.DEFAULT);
            if (response.isExists()) {
                long version = response.getVersion();
                String sourceAsString = response.getSourceAsString();
                logger.info("Document version: " + version);
                logger.info(sourceAsString);
            } else {
                logger.error(id + " does not exist");
            }
        } catch (ElasticsearchException e) {
            if (e.status() == RestStatus.NOT_FOUND) {
                logger.error("NOT FOUND", e);
            }
        }

    }

    public RestHighLevelClient createElasticSearchClient() {
        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost("localhost", 9200, "http")));
    }

    public static void main(String[] args) throws IOException {
        new ElasticsearchBasic().run();
    }

}
