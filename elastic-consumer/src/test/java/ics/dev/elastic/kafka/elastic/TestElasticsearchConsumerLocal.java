package ics.dev.elastic.kafka.elastic;

import com.google.gson.Gson;
import ics.dev.dm.TweetId;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestElasticsearchConsumerLocal {

    Gson gson = new Gson();

    @Test
    public void testGson() {
        String id = "{\"id_str\": \"1234\", \"another\": \"name\"}";
        TweetId tweetId = gson.fromJson(id, TweetId.class);
        assertEquals("1234", tweetId.getId_str());
    }
}
