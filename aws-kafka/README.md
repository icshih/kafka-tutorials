# AWS-Kafka

This module reads the files on AWS S3 object store to Kafka topics.

I use [Deutsche Börse Public Dataset](https://registry.opendata.aws/deutsche-boerse-pds/) which is freely available on AWS S3 as the source.

## Data Flow Schema

The production

    AWS S3 -> S3Producer -> Kafka
    
The consumption of the Kafka topic

    Kafka -> EurexConsumer
          -> EurexStream -> Kafka
          -> EurexSparkStream*
          
    * in module kafka-spark
    
## Data Model and Serialisation

The [data model](https://github.com/Deutsche-Boerse/dbg-pds/blob/master/docs/data_dictionary.md).

### Avro
To be able to serialise the data with Avro, it needs Schema Registry which is not included in the open source.  
 
