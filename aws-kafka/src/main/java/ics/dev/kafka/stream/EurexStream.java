package ics.dev.kafka.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class EurexStream {

    private final Logger logger = LoggerFactory.getLogger(EurexStream.class);
    private EurexStream() {}

    public KafkaStreams createKafkaStreams() {
        String bootstrapServers = "192.168.64.5:9092";
        // Create Properties
        Properties props = new Properties();
        props.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "eurex-streams");
        props.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
        props.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());

        // Create a topology
        StreamsBuilder builder = new StreamsBuilder();

        // Input topic
        KStream<String, String> inputTopic = builder.stream("eurex-test-full");
        // Process the input topic
        KStream<String, String> filteredStream = inputTopic.filter((k,v) -> {
            String secType = v.split(",")[5].replace("\"", "");
            return secType.equals("FUT");
        });
        // Route the processed data to another topic
        filteredStream.to("eurex-fut-only");

        // Build the topology
        return new KafkaStreams(builder.build(), props);
    }

    private void run() {
        // create a kafka streams
        logger.info("Create a Kafka streams");
        KafkaStreams stream = createKafkaStreams();
        stream.start();

        // add a shut down hook
        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Shutting down the streams application...");
            stream.close();
            logger.info("End of Application.");
        }));
    }

    public static void main(String[] args) {
        new EurexStream().run();
    }
}

