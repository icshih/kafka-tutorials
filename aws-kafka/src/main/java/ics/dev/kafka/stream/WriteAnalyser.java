package ics.dev.kafka.stream;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.utils.Bytes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.state.KeyValueStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;
import java.util.stream.Stream;

public class WriteAnalyser {

    final Logger logger = LoggerFactory.getLogger(WriteAnalyser.class);
    final CountDownLatch latch = new CountDownLatch(1);
    private WriteAnalyser() {}

    private KafkaStreams createKafkaStream() {
        String bootstrapServers = "192.168.64.9:9092";
        // Create Properties
        Properties props = new Properties();
        props.setProperty(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(StreamsConfig.APPLICATION_ID_CONFIG, "write-analysis-streams");
        props.setProperty(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());
        props.setProperty(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.StringSerde.class.getName());

        StreamsBuilder builder = new StreamsBuilder();
        KStream<String, String> source = builder.stream("write-input");
        source.flatMapValues(v -> Arrays.asList(v.toLowerCase(Locale.getDefault()).split("\\s+")))
                .groupBy((key, value) -> value)
                .count(Materialized.as("write-count-store"))
                .toStream()
                .to("analysis-output-2", Produced.with(Serdes.String(), Serdes.Long()));
        Topology topology = builder.build();
        logger.info(topology.describe().toString());
        return new KafkaStreams(topology, props);
    }

    private void dryRun() {
        KafkaStreams streams = createKafkaStream();
    }

    private void run() {
        KafkaStreams streams = createKafkaStream();

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            streams.close();
            latch.countDown();
        }));

        try {
            streams.start();
            latch.await();
        } catch (InterruptedException e) {
            logger.warn("Program interrupted, ", e);
        }

    }

    public static void main(String[] args) {
        new WriteAnalyser().run();
    }
}
