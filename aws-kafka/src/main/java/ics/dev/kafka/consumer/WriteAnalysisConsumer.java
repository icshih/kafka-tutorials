package ics.dev.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class WriteAnalysisConsumer {

    static final Logger logger = LoggerFactory.getLogger(WriteAnalysisConsumer.class);

    public static void main(String[] args) {
        new WriteAnalysisConsumer().run();
    }

    private WriteAnalysisConsumer() {
    }

    private void run() {
        String bootstrapServers = "192.168.64.9:9092";
        String groupId = "write-analyser-3";
        String topic = "analysis-output-2";

        Properties props = new Properties();
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, LongDeserializer.class.getName());
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        logger.info("Create a consumer...");
        KafkaConsumer<String, Long> consumer = new KafkaConsumer<>(props);

        logger.info("Creating consumer thread");
        ConsumerRunner runner = new ConsumerRunner(consumer, topic, new CountDownLatch(1));

        logger.info("Starting the consumer thread");
        Thread consumerThread = new Thread(runner);
        consumerThread.start();

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Receive shutdown hook");
            runner.shutdown();
        }));
    }

    public static class ConsumerRunner implements Runnable {

        private final KafkaConsumer<String, Long> consumer;
        private final String topic;
        private final CountDownLatch latch;
        private final Logger logger = LoggerFactory.getLogger(this.getClass());

        public ConsumerRunner(KafkaConsumer<String, Long> consumer,
                              String topic, CountDownLatch latch) {
            this.consumer = consumer;
            this.topic = topic;
            this.latch = latch;
        }

        @Override
        public void run() {
            try {
                consumer.subscribe(Arrays.asList(topic));

                while(latch.getCount() > 0) {
                    ConsumerRecords<String, Long> records = consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, Long> record : records) {
                        Long counts = record.value();
                        System.out.println(String.format("%s %d", record.key(), counts));
                    }
                    consumer.commitAsync((offset, e) -> {
                        if (e != null)
                            logger.error("Commit failed for offsets {}", offset, e);
                    });
                }
            } catch (WakeupException e) {
                logger.warn("Receive shutdown signal.");
            } finally {
                consumer.close();
            }
        }

        // Shutdown hook which can be called from a separate thread
        public void shutdown() {
            latch.countDown();
            // The wakeup() method is a special method to interrupt consumer.poll()
            // It throws WakeupException
            consumer.wakeup();
        }
    }
}

