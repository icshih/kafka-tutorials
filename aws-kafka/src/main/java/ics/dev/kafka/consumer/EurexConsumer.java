package ics.dev.kafka.consumer;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

public class EurexConsumer {

    static final Logger logger = LoggerFactory.getLogger(EurexConsumer.class);

    public static void main(String[] args) {
        new EurexConsumer().run();
    }

    private EurexConsumer() {
    }

    private void run() {
        String bootstrapServers = "192.168.64.5:9092";
        String groupId = "eurex-tester";
        String topic = "eurex-test-full";

        Properties props = new Properties();
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        logger.info("Create a consumer...");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);

        logger.info("Creating consumer thread");
        ConsumerRunner runner = new ConsumerRunner(consumer, topic);

        logger.info("Starting the consumer thread");
        Thread consumerThread = new Thread(runner);
        consumerThread.start();

        Runtime.getRuntime().addShutdownHook(new Thread( () -> {
            logger.info("Receive shutdown hook");
            runner.shutdown();
        }));

        try {
            consumerThread.wait();
        } catch (InterruptedException e) {
            logger.warn("Application is interrupted", e);
        } finally {
            logger.info("Application is closing");
        }
    }

    public class ConsumerRunner implements Runnable {

        private final AtomicBoolean closed = new AtomicBoolean(false);
        private final KafkaConsumer<String, String> consumer;
        private final String topic;
        private final Logger logger = LoggerFactory.getLogger(this.getClass());

        public ConsumerRunner(KafkaConsumer<String, String> consumer,
                              String topic) {
            this.consumer = consumer;
            this.topic = topic;
        }

        @Override
        public void run() {
            try {
                consumer.subscribe(Arrays.asList(topic));

                while(!closed.get()) {
                    ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));
                    for (ConsumerRecord<String, String> record : records) {
                        String[] row = record.value().split(",");
                        String isin = row[0];
                        String date = row[12];
                        String time = row[13];
                        System.out.println(String.format("%s  %s  %s", isin, date, time));
                    }
                    consumer.commitAsync((offset, e) -> {
                        if (e != null)
                            logger.error("Commit failed for offsets {}", offset, e);
                    });
                }
            } catch (WakeupException e) {
                logger.warn("Receive shutdown signal.");
                if (!closed.get())
                    throw e;
            } finally {
                consumer.close();
            }
        }

        // Shutdown hook which can be called from a separate thread
        public void shutdown() {
            closed.set(true);
            // The wakeup() method is a special method to interrupt consumer.poll()
            // It throws WakeupException
            consumer.wakeup();
        }
    }
}

