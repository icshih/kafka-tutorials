package ics.dev.kafka.aws.s3;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.GetObjectRequest;
import software.amazon.awssdk.services.s3.model.ListObjectsRequest;
import software.amazon.awssdk.services.s3.model.S3Object;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class S3Tools {

    static final Logger logger = LoggerFactory.getLogger(S3Tools.class);
    static final S3Client client = S3Client.builder()
            .region(Region.EU_CENTRAL_1)
            .build();

    public static List<S3Object> listObjects(String bucketName) {
        ListObjectsRequest listObjects = ListObjectsRequest
                .builder()
                .bucket(bucketName)
                .build();
        return client.listObjects(listObjects).contents();
    }

    public static List<String> listKeys(String bucketName) {
        return listObjects(bucketName).stream().map(S3Object::key).collect(toList());
    }

    public static List<String> getLinesInObject(String bucketName, String key) {
        GetObjectRequest request = GetObjectRequest.builder()
                .bucket(bucketName)
                .key(key)
                .build();

        BufferedReader br = new BufferedReader(new InputStreamReader(client.getObject(request)));
        return br.lines()
                .skip(1)
                .map(String::toString)
                .collect(toList());

        //s3.getObject(request, ResponseTransformer.toFile(Paths.get("data", key.split("/")[1])));

    }

    public static void main(String[] args) {

        logger.info("AWS S3 example with Deutsche Börse Public Dataset");

        String bucketName = "deutsche-boerse-eurex-pds";
        listKeys(bucketName).forEach(logger::info);
    }
}
