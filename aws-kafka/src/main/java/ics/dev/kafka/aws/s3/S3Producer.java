package ics.dev.kafka.aws.s3;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class S3Producer {

    private final Logger logger = LoggerFactory.getLogger(S3Producer.class);

    private S3Producer() {}

    private void run() {
        logger.info("Ingest Deutsche Börse Public Dataset on AWS S3 to Kafka");

        logger.info("Create a Kafak producer");
        KafkaProducer<String, String> producer = createKafkaProducer();

        logger.info("Start AWS S3 connection");
        String bucketName = "deutsche-boerse-eurex-pds";
        S3Tools.listKeys(bucketName).parallelStream()
                .forEach(k -> sendTradeData(producer, bucketName, k));
        //sendTradeData(producer, bucketName, date);
    }

    private void sendTradeData(KafkaProducer<String, String> producer, String bucketName, String key) {
        logger.info(String.format("Download objects in %s with key %s", bucketName, key));
        String date = key.split("/")[0];
        S3Tools.getLinesInObject(bucketName, key)
                // send message asynchronously
                .forEach(l -> producer.send(new ProducerRecord<>("eurex-test-full", date, l), (recordMetadata, e) -> {
                    if (e != null) {
                        logger.error("Something goes wrong, ", e);
                    }
                }));
    }

    /*private void sendTradeData(KafkaProducer<String, String> producer, String bucketName, String date) {
        logger.info(String.format("Download objects in %s with key starting with %s", bucketName, date));
        S3Tools.listKeys(bucketName).stream()
                .filter(k -> k.startsWith(date))
                .flatMap(k -> S3Tools.getLinesInObject(bucketName, k).stream())
                // send message asynchronously
                .forEach(l -> producer.send(new ProducerRecord<>("eurex-test", date, l), (recordMetadata, e) -> {
                    if (e != null) {
                        logger.error("Something goes wrong, ", e);
                    }
                }));
    }*/

    private KafkaProducer<String, String> createKafkaProducer() {
        String bootstrapServers = "192.168.64.5:9092";
        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        // optimise data safety
        props.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        // use compression as often as possible
        props.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");
        props.setProperty(ProducerConfig.LINGER_MS_CONFIG, "20");
        props.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, Integer.toString(32*1024));
        return new KafkaProducer<>(props);
    }

    public static void main(String[] args) {
        new S3Producer().run();
    }
}
