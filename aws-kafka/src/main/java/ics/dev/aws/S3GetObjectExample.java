package ics.dev.aws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.ResponseInputStream;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.io.*;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class S3GetObjectExample {

    static final Logger logger = LoggerFactory.getLogger(S3GetObjectExample.class);

    public static void main(String[] args) {

        logger.info("AWS S3 example with Deutsche Börse Public Dataset");
        logger.info("Download objects...");

        String bucketName = "deutsche-boerse-eurex-pds";
        String key = "2017-07-07/2017-07-07_BINS_XEUR13.csv";
        S3Client client = S3Client.builder()
                                  .region(Region.EU_CENTRAL_1)
                                  // .credentialsProvider(ProfileCredentialsProvider.create())
                                  .build();

        List<String> lines = getLinesInObject(client, bucketName, key);
        lines.stream().forEach(System.out::println);
    }

    public static List<String> getLinesInObject(S3Client s3, String bucketName, String key) {
        GetObjectRequest request = GetObjectRequest.builder()
                    .bucket(bucketName)
                    .key(key)
                    .build();

        BufferedReader br = new BufferedReader(new InputStreamReader(s3.getObject(request)));
        return br.lines()
                .skip(1)
                .map(String::toString)
                .collect(toList());

        //s3.getObject(request, ResponseTransformer.toFile(Paths.get("data", key.split("/")[1])));

    }
}
