package ics.dev.aws;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.*;

import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

public class S3Example {

    static final Logger logger = LoggerFactory.getLogger(S3Example.class);

    public static void main(String[] args) {

        logger.info("AWS S3 example with Deutsche Börse Public Dataset");

        String bucketName = "deutsche-boerse-eurex-pds";
        S3Client client = S3Client.builder()
                                  .region(Region.EU_CENTRAL_1)
                                  // .credentialsProvider(ProfileCredentialsProvider.create())
                                  .build();
        listObjects(client, bucketName).stream().forEach(k -> logger.info(k));
    }

    public static List<String> listObjects(S3Client s3, String bucketName) {
        ListObjectsRequest listObjects = ListObjectsRequest
                .builder()
                .bucket(bucketName)
                .build();

        ListObjectsResponse listObjectsResponse = s3.listObjects(listObjects);
        List<S3Object> objects = listObjectsResponse.contents();
        return objects.stream().map(S3Object::key).collect(toList());
    }
}
