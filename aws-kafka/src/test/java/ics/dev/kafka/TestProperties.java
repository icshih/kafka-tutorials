package ics.dev.kafka;

import ics.dev.kafka.tools.KafkaTools;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Properties;

import static org.junit.Assert.*;


public class TestProperties {

    @Test
    public void testReadPropertiesFile() {
        File file = new File("src/test/resources/test.properties");
        Optional<Properties> opt = KafkaTools.GetProperties(file);
        assertTrue(opt.isPresent());
        Properties prop = opt.orElse(KafkaTools.getDefaultProperties());
        assertEquals("0.0.0.0:9092", prop.getProperty("local.bootstrapServers"));
    }

    @Test
    public void testReadPropertiesFileNonExist() {
        File file = new File("src/test/resources/no.properties");
        Optional<Properties> opt = KafkaTools.GetProperties(file);
        assertFalse(opt.isPresent());
        Properties prop = opt.orElse(KafkaTools.getDefaultProperties());
        assertEquals("127.0.0.1:9092", prop.getProperty("local.bootstrapServers"));
    }
}
