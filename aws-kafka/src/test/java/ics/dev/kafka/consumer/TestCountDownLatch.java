package ics.dev.kafka.consumer;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;

import static java.lang.Thread.sleep;

public class TestCountDownLatch {

    final static Logger logger = LoggerFactory.getLogger(TestCountDownLatch.class);
    CountDownLatch latch = new CountDownLatch(2);

    @Test
    public void test() {
        Thread thread1 = new Thread(() -> {
            logger.info("Thread 1 is waiting for countdown latch to become 0");
            try {
                latch.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            logger.info("Latch count is zero!");
        });

        thread1.start();

        while (latch.getCount() > 0) {
            try {
                sleep(1000);
                latch.countDown();
                logger.info("Latch count is {}", latch.getCount());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

}
