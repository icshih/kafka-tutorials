package ics.dev.kafka.aws.s3;

import org.junit.Ignore;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class TestS3Tools {

    final String bucketName = "deutsche-boerse-eurex-pds";

    @Test@Ignore
    public void testGetDate() {
        Map<String, List<String[]>> collect = S3Tools.listKeys(bucketName).stream().map(k -> k.split("/"))
                .collect(Collectors.groupingBy(k -> String.valueOf(k[0])));

        collect.keySet().forEach(System.out::println);
    }
}
