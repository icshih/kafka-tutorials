package ics.dev.kafka.stream;

import org.junit.Test;

import static org.junit.Assert.*;

public class TestStream {

    @Test
    public void testString() {
        String test = "FUT";
        assertTrue(test.equals("FUT"));
        assertEquals("FUT", test);
        assertFalse(test.equals("OPT"));
        assertNotEquals("OPT", test);
    }
}
