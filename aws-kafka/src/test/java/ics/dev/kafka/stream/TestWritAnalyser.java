package ics.dev.kafka.stream;

import org.junit.Test;

import java.util.Arrays;
import java.util.Locale;

public class TestWritAnalyser {

    @Test
    public void testSplit() {
        String test = "Je vous presente rapidement ma derniere mission chez Generali";
        String[] words = test.toLowerCase(Locale.getDefault()).split("\\s");
        Arrays.asList(words).stream().forEach(System.out::println);
    }
}
