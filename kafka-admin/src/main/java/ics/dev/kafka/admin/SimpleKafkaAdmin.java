package ics.dev.kafka.admin;

import org.apache.kafka.clients.admin.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SimpleKafkaAdmin {

    static final Logger logger = LoggerFactory.getLogger(SimpleKafkaAdmin.class);

    public static void main(String[] args) throws InterruptedException, ExecutionException, TimeoutException {

        String bootstrapServers = "192.168.64.5:9092";

        Properties props = new Properties();
        props.setProperty(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);

        AdminClient admin = KafkaAdminClient.create(props);
        ListTopicsResult topics = admin.listTopics();

        topics.listings().get(10000, TimeUnit.MILLISECONDS).stream()
                .map(TopicListing::name)
                .forEach(logger::info);
    }
}
