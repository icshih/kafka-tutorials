# Apache Spark Stream API

As at version 2.4.5, Apache Spark has two set of Stream APIs which correspond to the RDD and DataFrame/Dataset APIs.

In general, it is better to use the high-level __Structured Stream__ API rather than the low-level Spark Stream (DStream) API.
