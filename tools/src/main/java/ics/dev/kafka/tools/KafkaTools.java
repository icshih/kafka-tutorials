package ics.dev.kafka.tools;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Optional;
import java.util.Properties;

public class KafkaTools {

    private static Logger logger = LoggerFactory.getLogger(KafkaTools.class);

    public static Properties getDefaultProperties() {
        Properties prop = new Properties();
        prop.setProperty("local.bootstrapServers", "127.0.0.1:9092");
        return prop;
    }

    public static Optional<Properties> GetProperties(File file) {
        Properties prop = new Properties();
        try {
            InputStream is = new FileInputStream(file);
            prop.load(is);
            return Optional.of(prop);
        } catch (IOException e) {
            logger.error("Unable to load property file, see ", e);
            return Optional.empty();
        }
    }
}
