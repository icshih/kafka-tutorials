package ics.dev.kafka.tools;

import org.junit.Test;

import java.io.File;
import java.util.Optional;
import java.util.Properties;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class testKafkaTools {

    @Test
    public void testProperties() {
        File f1 = new File("src/test/resources/test.properties");
        Optional<Properties> test1 = KafkaTools.GetProperties(f1);
        assertTrue(test1.isPresent());

        File f2 = new File("src/test/resources/no.properties");
        Optional<Properties> test2 = KafkaTools.GetProperties(f2);
        assertFalse(test2.isPresent());
    }
}
