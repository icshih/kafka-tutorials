# Kafka Tutorials

This project contains Kafka applications based on the Udemy class, [**Apache Kafka Series - Learn Apache Kafka for Beginners v2**](https://aubay.udemy.com/course/apache-kafka/learn/).

## Pre-requisites

Be sure to install **Java 8+ SDK** and **Apache Maven** in the local development environment.

## Java 8+

All the code requires Java 8+ because of the usage of [lambda expressions](https://docs.oracle.com/javase/tutorial/java/javaOO/lambdaexpressions.html).

## Maven
The java package management and building system.

To build the executable jar file, simply go to the root directory of the package

    mvn clean package 